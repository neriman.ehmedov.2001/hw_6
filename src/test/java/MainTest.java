import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    public Family family;
    private Human father;
    private Human mother;
    private Human testch1;
    private Human testch2;
    private Human testch3;
    Pet pet;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Rahima", "Ahmadov", 2000);
        father = new Human("Neriman", "Ahmadov", 2001);
        family = new Family(mother, father);
        Pet esmasPet = new Pet("dog", "Rex",
                5, 49, new String[]{"eat, drink, sleep"});
        family.setPet(esmasPet);
        testch1 = new Human("TestChild1", "Ban", 2003);
        testch2 = new Human("TestChild2", "Banuu", 2010);
        testch3 = new Human("TestChild3", "MelisBanu", 1999);
        family.addChild(testch1);
        family.addChild(testch2);
    }

    @Test
    void testToString() {
        String expectedOutput = "Family{mother=Human{name= 'Rahima', " + "surname='Ahmadov', year= 2000, " +
                "iq= 100, schedule=null}, " +
                "father=Human{name= 'Neriman', surname= 'Ahmadov', year= 2001, iq= 100, schedule=null}, " +
                "pet=Dog{nickname='Rex, age=5, trickLevel=49, habits=[eat, drink, sleep]}, " +
                "child=[Human{name='TestChild1', surname='Ban', year=2003, iq=0, schedule=null}, " +
                "Human{name='TestChild2', surname='Banuu', year=2010, iq=0, schedule=null}]}";
        assertEquals(expectedOutput, family.toString());
    }

    @Test
    void addChild() {
        family.addChild(testch3);
        assertTrue(family.getChild().contains(testch3));
    }

    @Test
    void deleteExistingObjectChild() {
        assertTrue(family.deleteChild(testch1));
        assertFalse(family.getChild().contains(testch1));
    }

    @Test
    void deleteNonExistingObjectChild() {
        assertFalse(family.deleteChild(testch3));
    }

    @Test
    void deleteExistingIndexChild() {
        assertTrue(family.deleteChild(2));
        assertFalse(family.getChild().contains(testch1));
    }

    @Test
    void deleteNonExistingIndexChild() {
        assertFalse(family.deleteChild(10));
    }

    @Test
    void countFamily() {
        int result = family.countFamily();
        assertEquals(1, result);
    }
}