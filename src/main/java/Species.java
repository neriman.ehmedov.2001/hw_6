import java.util.Arrays;
import java.util.Objects;
import java.util.Enumeration;

enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    UNKNOWN;
}