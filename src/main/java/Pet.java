import java.util.Arrays;

class Pet {
    // field
    private String nickname;
    private String[] habits;
    private int age, trickLevel;
    private Species species;

    // constructors
    public Pet() {
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        switch(species.toLowerCase()){
            case "dog":
                this.species = Species.Dog;
                break;
            case "domesticCat":
                this.species = Species.DomesticCat;
                break;
            case "roboCat":
                this.species = Species.RoboCat;
                break;
            case "fish":
                this.species = Species.Fish;
                break;
        }
    }

    // methods
    public void eat() {
        System.out.println("I am eating.");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up.");
    }

    // Override methods
    @Override
    public String toString() {
        return species + "{" +
                "nickname = '" + nickname + '\'' +
                ", age = " + age +
                ", trickLevel = " + trickLevel +
                ", habits = " + Arrays.toString(habits) + "}";
    }

    // setters
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setSpecies(String species) {
        switch(species){
            case "dog":
                this.species = Species.Dog;
                break;
            case "domesticCat":
                this.species = Species.DomesticCat;
                break;
            case "doboCat":
                this.species = Species.RoboCat;
                break;
            case "fish":
                this.species = Species.Fish;
                break;
            default:
                this.species = Species.UNKNOWN;
                break;
        }
    }

    // getters
    public String getNickname() {
        return nickname;
    }
    public String getHabits() {
        return Arrays.toString(habits);
    }
    public int getAge() {
        return age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public String getSpecies() {
        switch(species){
            case Dog:
                return "Dog";
            case DomesticCat:
                return "DomesticCat";
            case RoboCat:
                return "RoboCat";
            case Fish:
                return "Fish";
            default:
                return "UNKNOWN";
        }
    }
}