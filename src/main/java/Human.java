import java.util.stream.IntStream;

class Human implements CharSequence {
    // field
    private String name, surname;
    private int year, iq;
    private Family family;
    public DayOfWeek monday = DayOfWeek.MONDAY;;
    public DayOfWeek tuesday = DayOfWeek.TUESDAY;;
    public DayOfWeek wednesday = DayOfWeek.WEDNESDAY;;
    public DayOfWeek thursday = DayOfWeek.THURSDAY;;
    public DayOfWeek friday = DayOfWeek.FRIDAY;;
    public DayOfWeek saturday = DayOfWeek.SATURDAY;;
    public DayOfWeek sunday = DayOfWeek.SUNDAY;;

    // constructors
    public Human() {
    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    // methods
    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname() + '.');
    }
    public void describePet() {
        System.out.print("I have a " + family.getPet().getSpecies() +
                ", he is " + family.getPet().getAge() + " years old, he is ");
        System.out.println((family.getPet().getTrickLevel() > 50) ? "very sly." : "almost not sly.");
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return null;
    }

    // Override methods
    @Override
    public String toString() {
        return "Human{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", year = " + year +
                ", iq = " + iq +
                ", schedule = " + monday + " " + monday.get() + " "
                + tuesday + " " + tuesday.get() + " "
                + wednesday + " " + wednesday.get() + " "
                + thursday + " " + thursday.get() + " "
                + friday + " " + friday.get() + " "
                + saturday + " " + saturday.get() + " "
                + sunday + " " + sunday.get() + "}";
    }

    @Override
    public IntStream chars() {
        return null;
    }

    @Override
    public IntStream codePoints() {
        return null;
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setSchedule(String schedule, String doing){
        switch(schedule.toUpperCase()){
            case "MONDAY":
                this.monday.set(doing);
                break;
            case "TUESDAY":
                this.tuesday.set(doing);
                break;
            case "WEDNESDAY":
                this.wednesday.set(doing);
                break;
            case "THURSDAY":
                this.thursday.set(doing);
                break;
            case "FRIDAY":
                this.friday.set(doing);
                break;
            case "SATURDAY":
                this.saturday.set(doing);
                break;
            case "SUNDAY":
                this.sunday.set(doing);
                break;
            default:
                System.out.println("Please write correct days of the week");
                break;
        }
    }

    // getters
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getYear() {
        return year;
    }
    public int getIq() {
        return iq;
    }
    public Family getFamily() {
        return family;
    }
    public String getSchedule() {
        return monday + " " + monday.get() + " "
                + tuesday + " " + tuesday.get() + " "
                + wednesday + " " + wednesday.get() + " "
                + thursday + " " + thursday.get() + " "
                + friday + " " + friday.get() + " "
                + saturday + " " + saturday.get() + " "
                + sunday + " " + sunday.get();
    }
}