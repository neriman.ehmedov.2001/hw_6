enum DayOfWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;

    private String label;

    public void set (String label){
        this.label = label;
    }

    public String get(){
        return label;
    }
}
