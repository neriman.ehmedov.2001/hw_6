public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Rahima", "Ahmadov", 2000);
        Human father = new Human("Neriman", "Ahmadov", 2001);

        Family familyAhamdov = new Family(mother, father);
        mother.setFamily(familyAhamdov);
        father.setFamily(familyAhamdov);

        Human esma = new Human("Esma", "Ahmadov", 2020, 160);

        System.out.println(esma.monday.name());     esma.setSchedule("Monday", "go to courses; watch a film");
        System.out.println(esma.tuesday.name());    esma.setSchedule("Sunday", "do home work");
        System.out.println(esma.wednesday.name());  esma.setSchedule("Wednesday", "do workout");
        System.out.println(esma.thursday.name());   esma.setSchedule("Friday", "read e-mails");
        System.out.println(esma.friday.name());     esma.setSchedule("Saturday", "do shopping");
        System.out.println(esma.saturday.name());   esma.setSchedule("Thursday", "visit grandparents");
        System.out.println(esma.sunday.name());     esma.setSchedule("Tuesday", "do household");

        familyAhamdov.addChild(esma);
        esma.setFamily(familyAhamdov);

        Pet esmasPet = new Pet("dog", "Rex",
                5, 49, new String[]{"eat, drink, sleep"});
        familyAhamdov.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Human child1 = new Human();
        Human child2 = new Human();
        Human child3 = new Human();
        familyAhamdov.addChild(child1);
        familyAhamdov.addChild(child2);
        familyAhamdov.addChild(child3);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(2);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(child3);
        familyAhamdov.countFamily();
    }
}